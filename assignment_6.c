#include<stdio.h>
#include<stdlib.h>

struct node{
    int roll, std;
    char name[20];
    char subj_names[6][20];
    int marks[6];
    struct node* next;
};

struct node *front=NULL, *rear=NULL;

void traverse();
void enqueue();
void dequeue();
void peek();
int menu();

int main(){
    int choice=0;
    while(1){
        choice = menu();

        switch(choice){
            case 0:
                exit(0);
                break;

            case 1:
                traverse();
                break;

            case 2:
                enqueue();
                break;

            case 3:
                dequeue();
                break;

            case 4:
                peek();
                break;

            default:
                printf("\nEnter a vaild choice.");

        }
        getch();
    }

    return 0;
}

void enqueue(){
    struct node* newnode = (struct node*)(malloc(sizeof(struct node)));
    if(sizeof(newnode)>0){

        printf("\nEnter roll: ");
        scanf("%d",&newnode->roll);
        fflush(stdin);

        printf("\nEnter name: ");
        scanf("%s", newnode->name);
        fflush(stdin);

        printf("\nEnter standard: ");
        scanf("%d",&newnode->std);
        fflush(stdin);

        for(int i=0; i<6; i++){
            printf("\nEnter #%d subject name: ", i);
            scanf("%s", newnode->subj_names[i]);
            fflush(stdin);

            printf("\nEnter its marks: ");
            scanf("%d",&newnode->marks[i]);
            fflush(stdin);

        }

        newnode->next=NULL;
    }
    if(front==NULL && rear==NULL){
        front = rear = newnode;
        rear->next=front;
    }
    else{
        rear->next=newnode;
        rear = newnode;
        rear->next = front;
    }
    printf("\nNew node inserted.");
}

void dequeue(){
    struct node *temp=front;


    if(front==NULL && rear==NULL)
        printf("\nNo item in the queue.");

    else if(front == rear && front!=NULL){ //Queue has only 1 element.
        printf("\nRoll: %d \nName: %s \nStd: %d \n[Subj,marks]: \n", front->roll, front->name, front->std);
        for(int i=0; i<6;i++)
            printf(" [%s, %d]\n", front->subj_names[i], front->marks[i]);

        free(temp);
        front = rear = NULL;
    }
    else{
        printf("\nRoll: %d \nName: %s \nStd: %d \n[Subj,marks]: \n", front->roll, front->name, front->std);
        for(int i=0; i<6;i++)
            printf(" [%s, %d]\n", front->subj_names[i], front->marks[i]);

        front = front->next;
        free(temp);
        rear->next = front;
    }

    printf("\nNode removed.");
}

void traverse(){
    struct node *iter=front;
    system("cls");
    if(front==NULL&&rear==NULL){
        printf("\n!!* Queue is empty *!!");
    }

    else{
        printf("\nItems in queue are: \n");
        for(iter=front; iter->next!=front;iter=iter->next){
            printf("\nRoll: %d \nName: %s \nStd: %d\n[Subj, marks]: \n", iter->roll, iter->name, iter->std);
            for(int i=0; i<6;i++)
                printf(" [%s, %d]\n", iter->subj_names[i], iter->marks[i]);

        printf("--------------------------------------------");
        }

        printf("\nRoll: %d \nName: %s \nStd: %d\n[Subj, marks]: \n", iter->roll, iter->name, iter->std);
        for(int i=0; i<6;i++)
            printf("[%s, %d]\n", iter->subj_names[i], iter->marks[i]);


    }
}

void peek(){
    if(front==NULL){
        printf("\n!!* Queue is empty. *!!");
    }
    else{
        printf("\n1st item in queue: \nRoll: %d \nName: %s \nStd: %d \n[Subj,marks]: \n", front->roll, front->name, front->std);
        for(int i=0; i<6;i++)
            printf(" [%s, %d]\n", front->subj_names[i], front->marks[i]);
    }
}

int menu(){
    int choice=0;
    system("cls");
    printf("\n--Welcome--\n0. Exit\n1. Show all items in queue\n2. Enqueue an item\n3. Dequeue an item\n4.Peek in to queue\nEnter Choice: ");
    scanf("%d",&choice);
    return choice;
}