#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<windows.h>
struct Employee
{
    int id;
    char name[100];
    char address[100];
    int salary;
    char Dob[15];
    char doj[15];
};
float age_decimal(int ddj,int mmj,int ddb,int mmb,int yyj);
int is_leap(int yyj);
int switch_days(int mmj,int yyj);
int main()
{
    SYSTEMTIME stime;
    GetSystemTime(&stime);
    struct Employee e;
    printf("Enter the Employee ID: \n");
    scanf("%d",&e.id);
    fflush(stdin);
    printf("Enter the Employee Name: \n");
    scanf("%s",e.name);
    fflush(stdin);
    printf("Enter Employee Address:  \n");
    scanf("%s",e.address);
    fflush(stdin);
    printf("Enter your birth date in dd/mm/yyyy format: \n");
    scanf("%s",e.Dob);
    fflush(stdin);
    printf("Enter your date of joining in dd/mm/yyyy format: \n");
    scanf("%s",e.doj);
    fflush(stdin);
    printf("Enter Employee salary: \n");
    scanf("%d",&e.salary);
    fflush(stdin);
    char ddbs[3],mmbs[3],yybs[5];
    strcpy(ddbs,strtok(e.Dob,"/"));
    strcpy(mmbs,strtok(NULL,"/"));
    strcpy(yybs,strtok(NULL,"/"));
    int ddb=atoi(ddbs);
    int mmb=atoi(mmbs);
    int yyb=atoi(yybs);
    char ddjs[3],mmjs[3],yyjs[5];
    strcpy(ddjs,strtok(e.doj,"/"));
    strcpy(mmjs,strtok(NULL,"/"));
    strcpy(yyjs,strtok(NULL,"/"));
    int ddj=atoi(ddjs);
    int mmj=atoi(mmjs);
    int yyj=atoi(yyjs);

    float age;
    if(mmj>=mmb)
        age=(float)yyj-yyb+age_decimal(ddj,mmj,ddb,mmb,yyj);
    else
        age=(float)(yyj-yyb-1)+age_decimal(ddj,mmj,ddb,mmb,yyj);
    float exp;
    if(stime.wMonth>=mmj)
    {
        exp=(float)(stime.wYear-yyj)+(stime.wMonth-mmj)/12.0;
    }
    else
    {
        exp=(float)(stime.wYear-yyj)-(stime.wMonth-mmj)/12.0;

    }
    int daysPerMonth[12]= {31,28,31,30,31,30,31,31,30,31,30,31};
    int daysPerMonthLeap[12]= {31,29,31,30,31,30,31,31,30,31,30,31};
    int day=ddj;
    int month=mmj;
    int year=yyj;
    int i;
    for(i=1; i<=90; i++)
    {
        day++;
        if(is_leap(yyj))
        {
            if(day>daysPerMonthLeap[month-1])
            {
                day=1;
                month++;
                if(month==13)
                   {
                       month=1;
                       year++;
                   }
            }
        }
        else
        {
            if(day>daysPerMonth[month-1])
            {
                day=1;
                month++;
                if(month==13)
                 {
                    month=1;
                    year++;
                 }
            }
        }
    }
    char prob[20];
    sprintf(prob,"%d/%d/%d",day,month,year);
    printf("The age of the Employee at the time joining the Company is %0.1f years\n",age);
    printf("The employee experience till date is %0.2f years\n",exp);
    printf("Probabation Completion Date: %s\n",prob);
    return 0;
}
float age_decimal(int ddj,int mmj,int ddb,int mmb,int yyj)
{
    int day;
    if(mmj>mmb)
    {
        day=(switch_days(mmj,yyj)+ddj)-(switch_days(mmb,yyj)+ddb);

    }
    else
    {

        int x;
        day=(switch_days(mmj,yyj)+ddj);
        if(is_leap(yyj-1))
            x=(366-(switch_days(mmb,yyj-1)+ddb));
        else
            x=(365-(switch_days(mmb,yyj-1)+ddb));
        day+=x;
    }
    if(is_leap(yyj))
        return (day/366.0);
    else
        return (day/365.0);
}
int is_leap(int yyj)
{
    if(yyj%4==0)
    {
        if(yyj%100==0)
        {
            if(yyj%400==0)
                return 1;
            else
                return 0;
        }
        else
            return 1;
    }
    else
        return 0;
}
int switch_days(int mmj,int yyj)
{
    int day;
    switch(mmj-1)
    {
    case 1:
        day=31;
        break;
    case 2:
        is_leap(yyj)?(day=60):(day=59);
        break;
    case 3:
        is_leap(yyj)?(day=91):(day=90);
        break;
    case 4:
        is_leap(yyj)?(day=121):(day=120);
        break;
    case 5:
        is_leap(yyj)?(day=152):(day=151);
        break;
    case 6:
        is_leap(yyj)?(day=182):(day=181);
        break;
    case 7:
        is_leap(yyj)?(day=213):(day=212);
        break;
    case 8:
        is_leap(yyj)?(day=244):(day=243);
        break;
    case 9:
        is_leap(yyj)?(day=274):(day=273);
        break;
    case 10:
        is_leap(yyj)?(day=305):(day=304);
        break;
    case 11:
        is_leap(yyj)?(day=335):(day=334);
        break;
    default:
        return 0;
    }
    return day;
}
